# language-startup-benchmark

When using a script, the speed the language starts up matters the most. Regardless of how fast it can process data, a language that starts up slower will end up slower for a simple script.

This project aims to run a simple benchmark with hello world for many languages.

## Results on my machine

![histogram](results/language-startup-times.png)

Text report. Note that this is averaged from about a hundred 3 digit floats (0.001).

```json
{
    "gcc": 0.0009760479041916175,
    "nim": 0.0010778443113772464,
    "rust": 0.0014011976047904203,
    "vala": 0.0014670658682634742,
    "lua": 0.0015149700598802406,
    "picolisp (kernel only)": 0.0016167664670658694,
    "go": 0.0016167664670658694,
    "perl": 0.002281437125748505,
    "haskell": 0.0025988023952095824,
    "bash": 0.004119760479041919,
    "zsh": 0.0042035928143712604,
    "picolisp (with pil)": 0.005083832335329345,
    "sbcl": 0.0070958083832335355,
    "gauche scheme": 0.012443113772455078,
    "guile scheme": 0.015622754491017946,
    "python": 0.02737125748502994,
    "gjs": 0.042371257485029915,
    "clisp": 0.04469461077844309,
    "php": 0.05075449101796407,
    "java": 0.05608383233532936,
    "ruby": 0.06107784431137722,
    "emacs lisp": 0.06334730538922152,
    "fish": 0.06509580838323355,
    "node": 0.06655089820359282,
    "racket/base": 0.06997005988023951,
    "io": 0.0874431137724551,
    "chez scheme": 0.1614790419161677,
    "go (go run)": 0.1885089820359281,
    "racket": 0.3612395209580839,
    "clojure": 0.9790538922155686
}
```

## Startup time for compiled languages

For compiled languages, I'm measuring the speed the compiled executable runs.

## Running

- Run `language-startup-benchmark` for a few times to time the languages and generate reports.

```bash
for i in $(seq 0 100); do
    language-startup-benchmark _reports/$(date --iso-8601=seconds).json
done
```

- Sometimes some compiles will fail. Until I actually manage to fix it or handle it in `process.rkt`, just remove all reports with failures.

```bash
rm "$(grep failed _reports/* --files-with-match)"
```

- Run `process.rkt` to generate a text report, an averaged json report, and a histogram from the files in the folder.

```bash
racket process.rkt _reports
```

Licensed under the MIT license.
